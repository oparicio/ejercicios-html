// EJERCICIO 1 //
$(document).on("click", ".cuadrados", function() {
  $(this).toggleClass("circulos");
});

function accion_add() {
  $(".cuadrados").addClass("poner_blue");
}
function accion_remove() {
  $(".cuadrados").removeClass("poner_blue");
}

$(document).on("click", "#poner_color", accion_add);
$(document).on("click", "#quitar_color", accion_remove);

// EJERCICIO 2 //

function copia() {
  let copy_text = $("#original").val();
  $("#copy").val(copy_text);
}

$(document).on("click", "#copy_button", copia);

// EJERCICIO 3 //

function copia2() {
  let copy_text2 = $("#original2").val();
  $(".texto_celda").text(copy_text2);
}

$(document).on("click", "#copy_button2", copia2);

function addtext() {
  let copy_text2 = $("#original2").val();
  $("p").append(copy_text2); // AÑADIR TEXTO
}
$(document).on("click", "#copy_button_add", addtext);

// EJERCICIO 4 //
let marcador = $(".marcador"); // Definim la clase a una var

function sumanum() {
  if (marcador.val() < 10) {
    let valor = parseInt(marcador.val()) + 1;
    marcador.val(valor);
  }
}

function restanum() {
  if (marcador.val() > 0) {
    let valor = parseInt(marcador.val()) - 1;
    marcador.val(valor);
  }
}

$(document).on("click", ".button2", sumanum);
$(document).on("click", ".button1", restanum);

// EJERCICIO 5 //

function sumanum2() {
  $(".marcador2").each(function(index) {
    let marcadortype = ".marcador2:nth-of-type(";
    let marcador2 = $(marcadortype + (index + 1) + ")");

    let valor = parseInt(marcador2.val()) + 1;
    marcador2.val(valor);
  });
}
function restanum2() {
  $(".marcador2").each(function(index) {
    let marcadortype = ".marcador2:nth-of-type(";
    let marcador2 = $(marcadortype + (index + 1) + ")");

    let valor = parseInt(marcador2.val()) - 1;
    marcador2.val(valor);
  });
}

$(document).on("click", ".button4", sumanum2);
$(document).on("click", ".button3", restanum2);

// EJERCICIO 6 //
$(".marcador3:nth-of-type(even)").wrap("<a href=" + ">Test</a>");

function sumanum3() {
  $(".marcador3").each(function(index) {
    console.log(index);
    let marcadortype = ".marcador3:nth-of-type(";
    let marcador3 = $(marcadortype + (index + 1) + ")");
    let valor = parseInt(marcador3.val());

    if (valor < 10) {
      valor++;
      console.log(marcador3);
    }

    marcador3.val(valor);
  });
}
function restanum3() {
  $(".marcador3").each(function(index) {
    let marcadortype = ".marcador3:nth-of-type(";
    let marcador3 = $(marcadortype + (index + 1) + ")");
    let valor = parseInt(marcador3.val());

    if (valor > 0) {
      valor--;
      console.log(marcador3);
    }
    marcador3.val(valor);
  });
}

$(document).on("click", ".button6", sumanum3);
$(document).on("click", ".button5", restanum3);

// EJERCICIO 8 //
// EN EL ARCHIVO

// EJERCICIO 9 //
// EN EL ARCHIVO

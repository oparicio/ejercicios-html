// EJEMPLO 1 //
function cambia() {
  let colors = ["yellow", "blue", "red", "green", "black"];
  let cuadrado = document.getElementById("este");

  let aleatorio = Math.floor(Math.random() * 5);
  let colorAleatorio = colors[aleatorio];

  cuadrado.style.backgroundColor = colorAleatorio;
}

// JQUERY //

function cambia2() {
  let cuadrados = document.getElementsByClassName("cuadrado");
  for (let x of cuadrados) {
    // con el of: X es cada uno de los elementos de la array
    x.style.backgroundColor = "blue";
  }
}

function cambia_jq() {
  $(".cuadrado").css("background-color", "yellow");
  // JQUERY - Con esto cogemos todos los elementos de la array y le aplicamos el css.
}
function cambia_jq_hide() {
  $(".cuadrado").hide();
}

function cambia_jq_show() {
  $(".cuadrado").show();
}

// Texto copia //

function copia() {
  let texto = $("#original").text();
  $("#copia").text(texto);
}

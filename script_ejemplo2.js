// add class //
function accion_add() {
  $("#parrafo").addClass("especial");
}
function accion_remove() {
  $("#parrafo").removeClass("especial");
}
function accion_toggle() {
  $("#parrafo").toggleClass("especial");
}

$(document).on("click", "#conmutador", accion_toggle);

$(document).on("click", ".cuadrado", function() {
  $(this).toggleClass("verde");
});

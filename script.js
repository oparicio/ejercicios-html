// EJERCICO 1 //
let numeros = [8, 16];

console.log(
  "El primer número és " + numeros[0] + " y el segón número és " + numeros[1]
);

//  EJERCICIO 2 //
let mayor = [3, 6, 1];
let numeromayor = 0;

for (let i = 0; i < mayor.length; i++) {
  if (mayor[i] > numeromayor) {
    numeromayor = mayor[i];
  }
}

console.log(" EL NUMERO MAYOR DEFINITIVO ES " + numeromayor);

// EJERCICIO 3 //
let num1 = 10;
let num2 = 8;

console.log(num1 + num2, num1 - num2, num2 * num1, num2 / num1);

// EJERCICIO 4 //
let chicos = 16;
let chicas = 11;
let totalclase = chicos + chicas;

console.log(
  "EL PORCENTAJE DE CHICAS EN CLASE ES DE " + (chicas / totalclase) * 100 + " %"
);

// EJERCICIO 5 //
let compara1 = 6;
let compara2 = 6;

if (compara1 > compara2) {
  console.log(compara1 + " ES MAYOR QUE " + compara2);
} else if (compara1 == compara2) {
  console.log(compara1 + " ES IGUAL QUE " + compara2);
} else {
  console.log(compara1 + " ES MENOR QUE " + compara2);
}

// EJERCICIO 6 //
let escogernum1 = prompt("Primer numero a operar:");
let escogernum2 = prompt("Segundo numero a operar:");
let escogerfuncion = prompt(
  "Si quieres sumar escribe 's', si quieres multiplicar escribe 'm'"
);

function operacion(num1, num2, op) {
  let num1Int = parseInt(num1);
  let num2Int = parseInt(num2);
  if (op == "s") {
    let result = num1Int + num2Int;
    console.log("La suma de " + num1 + " + " + num2 + " = " + result);
  } else if (op == "m") {
    let result = num1Int * num2Int;
    console.log("La multiplicación de " + num1 + "*" + num2 + " = " + result);
  } else {
    console.log("La operación es desconocida");
  }

  console.log("patata1");
}

operacion(escogernum1, escogernum2, escogerfuncion);

console.log("patata2");

// EJERCICIO 7 //
let year = prompt("Escribe el año que tu quieras:");
bisiesto(year);

function bisiesto(y1) {
  if (y1 % 4 == 0 && y1 % 100 != 0) {
    // % es modulo o residuo // // && es and
    console.log("El año " + y1 + " es bisiesto");
  } else if (y1 % 400 == 0) {
    console.log("El año " + y1 + " es bisiesto");
  } else {
    console.log("El año " + y1 + " no es bisiesto");
  }
}

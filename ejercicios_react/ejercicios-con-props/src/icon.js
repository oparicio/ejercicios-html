import React from "react";

const Icon = props => {
  let iconStyle = {
    color: props.color,
    position: props.position,
    right: props.right,
    top: props.top,
    width: props.width,
    height: props.height
  };
  return <i class="fa fa-paw" aria-hidden="true" style={iconStyle}></i>;
};

export default Icon;

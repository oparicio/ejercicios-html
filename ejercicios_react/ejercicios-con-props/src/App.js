import React from "react";
import "./css/App.css";
import Titulo from "./Titulo.js";
import Bola from "./Bola.js";
import Icon from "./icon.js";
import Capital from "./capital.js";
import BolaBingo from "./bolabingo.js";

function App() {
  //TITLE
  let textoTitulo = "Hola React!";

  //BALL
  let ballHeight = "80px";
  let ballWidth = "80px";
  let ballMargin = "8px";
  let ballColor = "red";
  let ballBorder = "50%";

  //ICON
  let color = "blue";
  let position = "absolute";
  let right = "10px";
  let top = "10px";
  let width = "40px";
  let height = "auto";
  return (
    <div className="App-header">
      <Titulo texto={textoTitulo} />
      <Bola // BOLA
        ballHeight={ballHeight}
        ballWidth={ballWidth}
        ballMargin={ballMargin}
        ballColor={ballColor}
        ballBorder={ballBorder}
      />
      <Icon
        color={color}
        position={position}
        right={right}
        top={top}
        width={width}
        height={height}
      />
      <div className="recuadro">
        <Capital display="display" nom="barcelona" />
      </div>
      <div className="bola-bingo">
        <BolaBingo num="22" />
      </div>
    </div>
  );
}

export default App;

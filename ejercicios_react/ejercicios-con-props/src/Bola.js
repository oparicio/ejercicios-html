import React from "react";

const Bola = props => {
  let estiloBola = {
    backgroundColor: props.ballColor,
    width: props.ballWidth,
    height: props.ballHeight,
    margin: props.ballMargin,
    borderRadius: props.ballBorder
  };
  return <div style={estiloBola}></div>;
};

export default Bola;

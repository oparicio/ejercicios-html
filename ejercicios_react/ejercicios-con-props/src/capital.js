import React from "react";
import "./css/recuadro.css";

let upperCaseStyle = {
  fontSize: "3em",
  margin: "0"
};
let lowCaseStyle = {
  margin: "0"
};
const capital = props => [
  <p style={upperCaseStyle}>{props.nom[0].toUpperCase()}</p>,
  <p style={lowCaseStyle}>
    {props.nom[0].toUpperCase()}
    {props.nom.substring(1, 9)}
  </p>
];

export default capital;

import React from "react";

const BolaBingo = props => {
  let numStyle = {
    color: "black",
    margin: "0"
  };
  return <h1 style={numStyle}>{props.num}</h1>;
};

export default BolaBingo;

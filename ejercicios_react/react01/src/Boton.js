import React from "react";

const Boton = props => {
  let botonText = props.encendido ? "Apagar" : "Encender";

  return <button onClick={props.pulsar}>{botonText}</button>;
};

export default Boton;

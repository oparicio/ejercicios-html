import React from "react";

import { CIUTATS } from "./datos.js";

class Ciudades extends React.Component {
  render() {
    // let ciudadesLi = CIUTATS.filter(
    //   (el) => el.toLowerCase()[0] === "b"
    // ).map((el, indice) => <li key={indice}>{el}</li>);

    let filas = CIUTATS.map((el, index) => (
      <tr key={index}>
        <td>{index}</td>
        <td>{el}</td>
      </tr>
    ));
    return (
      <>
        <h1>Ciudades</h1>
        <ul>{filas}</ul>
      </>
    );
  }
}

export default Ciudades;

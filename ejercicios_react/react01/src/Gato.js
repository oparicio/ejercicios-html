import React from "react";

function Gato200() {
  return (
    <>
      <img src="https://placekitten.com/200/200" />
    </>
  );
}
function Gato300() {
  return (
    <>
      <img src="https://placekitten.com/300/300" />
    </>
  );
}
export { Gato200, Gato300 };

// export default () => (
//   <>
//     <h1>Welcome to React Parcel Micro App!</h1>
//     <p>Hard to get more minimal than this React app.</p>
//   </>
// );

import React from "react";

// CSS links //
import "./css/cabecera.css";

// --------------- ESTO ES UNA CLASS ------------------ //

class CabeceraClass extends React.Component {
  render() {
    let estiloCabecera = {
      backgroundColor: this.props.color
    };
    return (
      <div className="cabecera" style={estiloCabecera}>
        {this.props.titulo}
      </div>
    );
  }
}

// --------------------- //

function Cabecera(props) {
  //indicamos props para decirle que usaremos props

  let estiloCabecera = {
    backgroundColor: props.color
  };
  return (
    <div className="cabecera" style={estiloCabecera}>
      {props.titulo}
    </div>
  );
}

export default Cabecera;

import React from "react";
import Bola from "./bola";

function Contenido(props) {
  //indicamos props para decirle que usaremos props

  function tituloRojo(props) {
    return <h1 style={{ color: "red" }}>{props.children}</h1>;
  }

  return (
    <>
      <tituloRojo>Capitulo 1</tituloRojo>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi, facere
        obcaecati alias animi veritatis minima officiis est delectus repellendus
        corrupti optio expedita vel laborum ut molestiae similique odio
        provident id.
      </p>
      <h1>Capitulo 2</h1>

      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi, facere
        obcaecati alias animi veritatis minima officiis est delectus repellendus
        corrupti optio expedita vel laborum ut molestiae similique odio
        provident id.
      </p>

      <Bola></Bola>
    </>
  );
}

export default Contenido;

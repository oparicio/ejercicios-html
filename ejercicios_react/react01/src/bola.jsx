import React from "react";
import "./css/bola.css";

class Bola extends React.Component {
  // ACTIVADOR //
  constructor(props) {
    super(props);

    this.state = {
      // Dar estilo //
      colorBola: "red"
    };

    // Necesario para asignar la funcion de clase //
    this.cambia = this.cambia.bind(this);
  }

  cambia() {
    if (this.state.colorBola == "red") {
      this.setState({ colorBola: "green" });
    } else {
      this.setState({ colorBola: "red" });
    }
  }

  render() {
    return (
      <div
        onClick={this.cambia}
        className="bola"
        style={{ backgroundColor: this.state.colorBola }}
      ></div>
    );
  }
}

export default Bola;

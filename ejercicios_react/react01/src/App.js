import React from "react";
// import { Gato200, Gato300 } from "./Gato";
import Cabecera from "./cabecera";
import Contenido from "./contenido";
import Fotografia from "./foto";
import { TituloFoto } from "./foto";
import Ciudades from "./ciudades";

import Luz from "./Luz";
import Bombilla from "./Bombilla";
import Boton from "./Boton";

// import Pie from "./pie";

function App() {
  let titulo = "Mi super app";
  return (
    <>
      <Cabecera titulo={titulo} color="red" />
      <Contenido />
      <TituloFoto />
      <Fotografia />
      <Luz />
      <Ciudades />
      {/* <Pie /> */}
    </>
  );
}
export default App;

// export default () => (
//   <>
//     <h1>Welcome to React Parcel Micro App!</h1>
//     <p>Hard to get more minimal than this React app.</p>
//   </>
// );

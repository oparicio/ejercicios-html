import React from "react";

// --- COMPOPNENTS SIN PROPS --- //

// export default () => <img src="http://lorempixel.com/100/100" />;

// OR (The best option)

const foto = () => <img src="http://placekitten.com/100/100" />;
export default foto;
// Este export, cuando lo importamos no ponemos {}

export const TituloFoto = () => <h1>Fotografia</h1>;
// Con este export, tenemos que poner {} para que busque el componente en concreto

// OR

// function foto() {
//   return <img src="http://lorempixel.com/100/100" />;
// }

// --- COMPOPNENTS CON PROPS --- //

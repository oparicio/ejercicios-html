import React from "react";
import { Bola } from "./Shapes.js";
import { Cuadrado } from "./Shapes.js";
import Separador from "./Separador.js";
import "./css/App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Hola Mundo</h1>
        <Bola />
        <Cuadrado />
        <Separador />
      </header>
    </div>
  );
}

export default App;

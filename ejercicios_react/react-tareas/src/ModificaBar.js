import React from "react";
import { Input, Button } from "reactstrap";

/*
Hacer que el boton de nueva tarea funcione
Mejorar presentación
Hacer que el input de color muestre selector de colores
...
*/

class ModificaBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      texto: this.props.textoM,
      color: this.props.colorM,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.cambiarTarea = this.cambiarTarea.bind(this);
  }

  handleInputChange(evento) {
    const target = evento.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  cambiarTarea() {
    //leer texto, color de state y llamar a this.props.cambiarTarea...
    this.props.cambiarTarea(this.state.texto, this.state.color);
    //console.log("creando tarea...");
  }

  render() {
    return (
      <>
        <h4>Modifica Tarea</h4>
        Tarea
        <Input
          type="text"
          value={this.state.texto}
          name="texto"
          onChange={this.handleInputChange}
        />
        <br />
        Color
        <Input
          type="color"
          value={this.state.color}
          name="color"
          onChange={this.handleInputChange}
        />
        <br />
        <Button onClick={this.cambiarTarea}>Guardar</Button>
      </>
    );
  }
}

export default ModificaBar;

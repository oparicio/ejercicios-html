import React from "react";
import { Container, Row, Col } from "reactstrap";
import "./css/app.css";

import Header from "./Header";
import SideBar from "./Sidebar";
import Content from "./Content";
import ModificaBar from "./ModificaBar.js";

const TAREAS_DEMO = [
  {
    id: 1,
    texto: "Comprar mascarilla",
    color: "#ff0000",
  },
  {
    id: 2,
    texto: "Hacer ejercicio TAREAS",
    color: "#00ff00",
  },
  {
    id: 3,
    texto: "Jugar GTA",
    color: "#0000ff",
  },
];

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tareas: TAREAS_DEMO,
      modificando: false,
      idM: "",
      textoM: "",
      colorM: "",
    };

    this.eliminaTarea = this.eliminaTarea.bind(this);
    this.nuevaTarea = this.nuevaTarea.bind(this);
    this.cambiarTarea = this.cambiarTarea.bind(this);
    this.modificaTarea = this.modificaTarea.bind(this);
  }

  nuevaTarea(texto, color) {
    let copyTask = JSON.parse(JSON.stringify(this.state.tareas));
    let nextId = this.state.tareas.length + 1;
    let newTask = { id: nextId, texto: texto, color: color };
    copyTask.push(newTask);

    this.setState({ tareas: copyTask });
  }

  eliminaTarea(idBorrar) {
    // eliminar tarea con id facilitado
    let copyTask = JSON.parse(JSON.stringify(this.state.tareas));
    copyTask = copyTask.filter((el) => el.id !== idBorrar);
    this.setState({ tareas: copyTask });
  }

  modificaTarea(tarea) {
    console.log("modificando id " + tarea.id);
    this.setState({
      modificando: true,
      idM: tarea.id,
      textoM: tarea.texto,
      colorM: tarea.color,
    });
  }

  cambiarTarea(texto_m, color_m) {
    let copyTask = JSON.parse(JSON.stringify(this.state.tareas));

    copyTask = copyTask.map((el) => {
      if (el.id === this.state.idM) {
        el.texto = texto_m;
        el.color = color_m;
      }
      return el;
    });

    this.setState({ tareas: copyTask, modificando: false });
  }

  render() {
    return (
      <>
        <Header />
        <Container>
          <Row>
            {this.state.modificando === true ? (
              <Col xs="3" className="modificabar">
                <ModificaBar
                  cambiarTarea={this.cambiarTarea}
                  textoM={this.state.textoM}
                  colorM={this.state.colorM}
                />
              </Col>
            ) : (
              <Col xs="3" className="sidebar">
                <SideBar nuevaTarea={this.nuevaTarea} />
              </Col>
            )}

            <Col className="content">
              <Content
                tareas={this.state.tareas}
                eliminaTarea={this.eliminaTarea}
                modificaTarea={this.modificaTarea}
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default App;

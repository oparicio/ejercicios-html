import React from "react";
import "./css/boton.css";

class Boton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      iconVisible: false,
      valor: ""
    };
    this.click = this.click.bind(this);
  }

  click() {
    if (this.state.iconVisible === false) {
      this.setState({ iconVisible: true, valor: this.props.turn });
      this.props.cambiaTurno(this.props.orden, this.props.turn);
    }
  }

  render() {
    let circulo = "fa fa-times icono";
    let cruz = "fa fa-circle-o icono";
    let iconClass = this.props.turn === "x" ? cruz : circulo;

    if (this.state.iconVisible === false) {
      iconClass = iconClass + " oculto";
    }

    return (
      <div className="boton" onClick={this.click}>
        <i className={iconClass} aria-hidden="true"></i>
      </div>
    );
  }
}

export default Boton;

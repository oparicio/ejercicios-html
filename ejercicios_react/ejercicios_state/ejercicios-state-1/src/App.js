import React from "react";
import "./App.css";
import { Container, Row, Col } from "reactstrap";
import Boton from "./boton.js";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      turn: "o",
      mapa: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    };
    this.cambiaTurno = this.cambiaTurno.bind(this);
    this.analizaMapa = this.analizaMapa.bind(this);
  }

  analizaMapa() {
    console.log("analizando jugadas...");
  }

  cambiaTurno(boton, jugada) {
    let nuevoTurno = this.state.turn === "x" ? "o" : "x";
    let posicion = boton - 1;
    jugada = jugada === "o" ? 1 : 2;
    let nuevoMapa = [...this.state.map];
    nuevoMapa[posicion] = jugada;
    this.setState({ map: nuevoMapa, turn: nuevoTurno }, this.analizaMapa);
  }

  render() {
    return (
      <Container>
        <p>Hola ReactStrap!</p>
        <Row>
          <Col>
            <Boton
              orden={1}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
          <Col>
            <Boton
              orden={2}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
          <Col>
            <Boton
              orden={3}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <Boton
              orden={4}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
          <Col>
            <Boton
              orden={5}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
          <Col>
            <Boton
              orden={6}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <Boton
              orden={7}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
          <Col>
            <Boton
              orden={8}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
          <Col>
            <Boton
              orden={9}
              cambiaTurno={this.cambiaTurno}
              turno={this.state.turn}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;

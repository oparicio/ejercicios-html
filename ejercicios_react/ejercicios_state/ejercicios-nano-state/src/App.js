import React from "react";
import "./css/App.css";
import Icon from "./icon.js";
import Tricolor from "./tricolor.js";

import { Container, Row, Col } from "reactstrap";
import Desplegable from "./desplegable";
import Lista_ciudades from "./listas-ciudades";

function App() {
  return (
    <div className="App">
      <Container>
        <Row>
          <Col>
            <Icon />
          </Col>
        </Row>
        <Row>
          <Col>
            <Tricolor />
          </Col>
        </Row>
        <Row>
          <Col>
            <Desplegable />
          </Col>
        </Row>
        <Row>
          <Col>
            <Lista_ciudades />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;

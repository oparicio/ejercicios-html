import React from "react";
import "./css/icon.css";

class Icon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      iconState: true
    };
    this.click = this.click.bind(this);
  }

  click() {
    if (this.state.iconState === true) {
      this.setState({ iconState: false });
    } else {
      this.setState({ iconState: true });
    }
  }
  render() {
    let iconUp = "fa fa-thumbs-o-up icon";
    let iconDown = "fa fa-thumbs-o-down icon";

    let iconPosition = this.state.iconState ? iconUp : iconDown;
    return (
      <i onClick={this.click} className={iconPosition} aria-hidden="true"></i>
    );
  }
}

export default Icon;

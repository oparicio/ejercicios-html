import React from "react";
import "./css/images.css";

class Desplegable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: 0,
    };
    this.click = this.click.bind(this);
    this.images = [
      "https://i.pinimg.com/736x/50/3e/9e/503e9ee3519d570e0330bd8720fcc302.jpg",
      "https://www.kienyke.com/sites/default/files/styles/amp_1200x675_16_9/public/wp-content/uploads/2018/08/Kawasaki-Ninja-H2R.jpg?itok=-g_VI5Ut",
      "https://www.mountainbike.es/media/cache/medium/upload/images/article/5b727d34a3fea541e6e08096/5e05eba20ce69404588b47b3-entrena-segun-tu-nivel.jpg",
      "https://i.pinimg.com/originals/74/c1/e1/74c1e186639ac1131f305294ecc7d009.jpg",
    ];
  }

  click(el) {
    let index = parseInt(el.target.value);
    this.setState({
      image: index,
    });
  }

  render() {
    return (
      <>
        <select onChange={this.click}>
          <option value="0">Coche</option>><option value="1">Moto</option>
          <option value="2">Bici</option>
          <option value="3">Bus</option>
        </select>
        <img className="images" src={this.images[this.state.image]} />
      </>
    );
  }
}

export default Desplegable;

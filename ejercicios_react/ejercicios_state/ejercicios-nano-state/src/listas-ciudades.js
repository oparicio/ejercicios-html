import React from "react";
import { Table } from "reactstrap";
import { CIUTATS_CAT_20K } from "./datos";
import "./css/tables-ciudades.css";

class Lista_ciudades extends React.Component {
  render() {
    let info1 = CIUTATS_CAT_20K.sort((a, b) =>
      a.municipi > b.municipi ? 1 : -1
    ).map((el, index) => (
      <tr>
        <th scope="row">{index}</th>
        <td>{el.municipi}</td>
        <td>{el.poblacio}</td>
        <td>{el.provincia}</td>
      </tr>
    ));

    let info2 = CIUTATS_CAT_20K.sort((a, b) =>
      a.poblacio > b.poblacio ? 1 : -1
    ).map((el, index) => (
      <tr>
        <th scope="row">{index}</th>
        <td>{el.municipi}</td>
        <td>{el.poblacio}</td>
        <td>{el.provincia}</td>
      </tr>
    ));
    return (
      <>
        <h1>ORDEN: ALFABETICO</h1>
        <Table hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Municipi</th>
              <th>Població</th>
              <th>Provincia</th>
            </tr>
          </thead>
          <tbody>{info1}</tbody>
        </Table>

        <br />
        <br />

        <h1>ORDEN: POBLACIÓN</h1>
        <Table hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Municipi</th>
              <th>Població</th>
              <th>Provincia</th>
            </tr>
          </thead>
          <tbody>{info2}</tbody>
        </Table>
      </>
    );
  }
}

export default Lista_ciudades;

import React from "react";
import "./css/tricolor.css";

class Tricolor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: 0
    };
    this.click = this.click.bind(this);
    this.colors = ["gray", "red", "green", "blue"];
  }

  click() {
    let newBackgroundColor =
      (this.state.backgroundColor + 1) % this.colors.length;
    this.setState({
      backgroundColor: newBackgroundColor
    });
  }

  render() {
    let currentColor = {
      backgroundColor: this.colors[this.state.backgroundColor]
    };

    return (
      <div className="tricolor" style={currentColor} onClick={this.click}></div>
    );
  }
}

export default Tricolor;
